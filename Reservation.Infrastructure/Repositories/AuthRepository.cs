﻿using Reservation.Data;
using Reservation.Models;
using Reservation.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.Repositories
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _dbContext;

        public AuthRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool IsUserExists(int id)
        {
            return _dbContext.Users.Any(u => u.Id_user == id);
        }

        public User Login(int id, string password)
        {
            var usr = _dbContext.Users.FirstOrDefault(u => u.Id_user == id);
            if (usr == null || !VerifyPasswordHash(password, usr.PasswordHash, usr.PasswordSalt))
                return null;
            return usr;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt);
            var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            return computedHash.SequenceEqual(passwordHash);
        }

        public User Register(User user, string password)
        {
            using var hmac = new System.Security.Cryptography.HMACSHA512();
            byte[] passwordSalt = hmac.Key;
            byte[] passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();
            
            return user;
        }
    }
}
