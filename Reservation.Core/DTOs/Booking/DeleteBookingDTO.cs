﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reservation.Core.DTOs.Booking
{
    public class DeleteBookingDTO
    {
        public int booking_id { get; set; }
    }
}
