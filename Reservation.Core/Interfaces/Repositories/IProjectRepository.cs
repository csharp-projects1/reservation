﻿using Reservation.Core.Interfaces.Repositories;
using Reservation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.Repositories.Interfaces
{
    public interface IProjectRepository : IEntityRepository<Project>
    {
        Task<bool> AllowProjectAsync(Guid id);
        Task<bool> IsProjectExists(Guid id);
        Task<IEnumerable<Project>> GetMyProjects(User u);
    }
}
